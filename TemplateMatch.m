%% Template Matching 
function[crosscollerate,imgs] = Templatematch(image,template)
img = {}
% Convert everything to Grayscale
    if size(image,3) == 3 % RGB images will have 3 entries in the third colum 
        image = rgb2gray(image);    
    end
    if size(template,3) == 3 % RGB images will have 3 entries in the third colum 
        template = rgb2gray(template);    
    end
    % Canny edge 
    img = edge(image, 'canny');
    nedg = edge(template, 'canny');
    imgs = normxcorr2(double(nedg),double(img)); % Convolute the two matricies
    %% Check if there was a point found in the picture
     bw = im2bw(imgs, 0.5);
     blank = im2double(false(size(bw,1),size(bw,2)));
     if bw == blank % There was no number plate
         disp('NO NUMBERPLATE WAS FOUND')
         
     else % Numberplate was probably found
         disp('NUMBER PLATE FOUND')
         xcorr = bw;
     end
    % threshold images
    % remove images that are now black
    % find the point in the image that is bright
% canny the image and find the area that the numberplate should take up
end