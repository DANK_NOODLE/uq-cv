%% Feature Matching 
Object = imread('logo.jpg');
figure()
imshow(Object)
%Load the Scene
scene= imread('Scene.jpg');
figure()
imshow(scene)
%convert to gray 
obj = rgb2gray(Object);
sc = rgb2gray(scene);
%Detect Feature points on object and scene
objpoints = detectSURFFeatures(obj);
scpoints = detectSURFFeatures(sc);
% Lets look at these points on the object. Show the top 100 
figure;
imshow(obj);
title('100 Strongest Feature Points');
hold on;
plot(selectStrongest(objpoints, 100));
% We want more in the scene as there is more clutter 
figure;
imshow(sc);
title('300 Strongest Feature Points from Scene Image');
hold on;
plot(selectStrongest(scpoints, 300));
%Now extract the features of these points, 
% This just returns what thefeatureis catagorisedby, its pretty much magic 
[objfeat, objpoints] = extractFeatures(obj, objpoints);
[scfeat, scpoints] = extractFeatures(sc, scpoints);
% We get the feature descriptor and also the coordinates
%Now we want to see if any features in these two images are simila 
pairs = matchFeatures(objfeat,scfeat);
%Lets have a look at these intial matches 
matchedObjPoints = objpoints(pairs(:, 1), :);
matchedScenePoints = scpoints(pairs(:, 2), :);
figure;
showMatchedFeatures(obj, sc, matchedObjPoints, ...
    matchedScenePoints, 'montage');
title('Putatively Matched Points (Including Outliers)');
%However we can be clever and estimate the transform (eg. affine) on the
%image to better look for the object, only points that are on this
%transform will work 
[tform, inlierObjPoints, inlierScenePoints] = ...
    estimateGeometricTransform(matchedObjPoints, matchedScenePoints, 'affine');
%Lets see what this ooks like....again 
figure;
showMatchedFeatures(obj, sc, inlierObjPoints, ...
    inlierScenePoints, 'montage');
title('Matched Points (Inliers Only)');

