%Exploring filters
clc, clear
%% Load ALL the images
img1 = imread('20980008.jpg');
ig = histeq(img1);
% Normal edging
is = edge(ig, 'sobel');
ic = edge(ig, 'canny');
tc = edge(img,'canny');
ts = edge(img, 'sobel');
G = fspecial('gaussian',[10 10],2);
tcb = imfilter(double(),G,'same');
tsb = imfilter(double(ts),G,'same');